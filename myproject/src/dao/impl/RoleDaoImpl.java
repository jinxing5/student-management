package dao.impl;

import bean.Menu;
import bean.Role;
import dao.DBUtils;
import dao.RoleDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoleDaoImpl extends DBUtils implements RoleDao {

    @Override
    public List<Role> GetRoleList(int pageIndex,int pageSize) {
        String sql = "select * from role limit ?,?";
        List list = new ArrayList();
        list.add((pageIndex-1)*pageSize);
        list.add(pageSize);

        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
            return null;
        }
        List<Role> roleList = new ArrayList<>();
        try {
            while (resultSet.next()){
                Role role = new Role();
                role.setState(resultSet.getInt("rolestate"));
                role.setRoleId(resultSet.getInt("roleid"));
                role.setRolename(resultSet.getString("rolename"));
                roleList.add(role);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return roleList;
    }

    @Override
    public List<Menu> GetMList() {
        String sql = "select * from menu";
        List list = new ArrayList();

        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
            return null;
        }
        List<Menu>menuList = new ArrayList<>();
        try {
            while (resultSet.next()){
                Menu menu = new Menu();
                menu.setDesc(resultSet.getString("desc"));
                menu.setMenuId(resultSet.getInt("menuid"));
                menu.setState(resultSet.getInt("state"));
                menu.setUpmenu(resultSet.getInt("upmenuid"));
                menu.setMenuName(resultSet.getString("menuname"));
                menu.setUrl(resultSet.getString("url"));
                menuList.add(menu);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return menuList;
    }

    @Override
    public boolean DeleteRole(int roleid) {
        String sql = "update role set rolestate=-1 where roleid=? ";
        List list =new ArrayList();
        list.add(roleid);
        getConnection();
        int count = update(sql,list);
        if(count==1){
            closeAll();
            return true;
        }
        closeAll();
        return false;
    }

    @Override
    public boolean DisabedRole(int roleid,int state) {
        String sql = "update role set rolestate=? where roleid=? ";
        List list =new ArrayList();
        list.add(state);
        list.add(roleid);
        getConnection();
        int count = update(sql,list);
        if(count==1){
            closeAll();
            return true;
        }
        closeAll();
        return false;
    }

    @Override
    public boolean UpdateRole(Role role) {
        String sql = "update role set rolename=?,rolestate=? where roleid=?";

        List list = new ArrayList();

        list.add(role.getRolename());
        list.add(role.getState());
        list.add(role.getRoleId());

        getConnection();
        int count = update(sql,list);
        if(count==1){
            closeAll();
            return true;
        }
        closeAll();
        return false;
    }

    @Override
    public boolean AddRole(Role role) {
        //1.对 role 表进行增加
        String sql1 = "insert into role values(null,?,?)";

        List list1 = new ArrayList();

        list1.add(role.getRolename());
        list1.add(role.getState());

        getConnection();
        int count1 = update(sql1,list1);
        closeAll();

        //2.对 middle 表进行增加:
        // 要利用 pps.getGeneratedKeys()方法 先获取新增role的数据的id值
        int newRoleId = 0;
        try {
            ResultSet generatedKeys = pps.getGeneratedKeys();
            newRoleId = 0;
            if(generatedKeys.next()) newRoleId=generatedKeys.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //批量新增
        if (role.getMenus()!=null) {
            String sql2 = "insert into middle values(null,?,?)";
            pps = getPps(sql2);
            try {
                for (Menu menu:role.getMenus()
                     ) {
                    pps.setInt(1,newRoleId);
                    pps.setInt(2,menu.getMenuId());
                    pps.addBatch();
                }
                pps.executeBatch();
            } catch (SQLException e) {
                e.printStackTrace();
            }finally {
                closeAll();
            }
        }
        if(count1==1){
            closeAll();
            return true;
        }
        return false;
    }

    @Override
    public int CountRole() {
        String sql = "select count(*) from role";
        List list = new ArrayList();

        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
        }

        int count = 0;
        try {
            while (resultSet.next()){
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return count;
    }

    @Override
    public Role FindRoleById(int roleid) {
        String sql = "select * from role where roleid=?";
        List list = new ArrayList();
        list.add(roleid);

        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
        }

        Role role = new Role();
        try {
            while (resultSet.next()){
                role.setRoleId(resultSet.getInt("roleid"));
                role.setRolename(resultSet.getString("rolename"));
                role.setState(resultSet.getInt("rolestate"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return role;
    }

}

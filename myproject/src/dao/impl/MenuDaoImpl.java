package dao.impl;

import bean.Menu;
import bean.Role;
import dao.DBUtils;
import dao.MenuDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MenuDaoImpl extends DBUtils implements MenuDao {
    @Override
    public boolean UpdateMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean AddMenu(Menu menu) {
        return false;
    }

    @Override
    public int CountMenu() {
        return 0;
    }

    @Override
    public Role FindMenuById(int menuid) {
        return null;
    }

    @Override
    public List<Menu> GetMenuList(int pageIndex,int pageSize) {
        String sql = "select * from menu limit ?,?";
        List list = new ArrayList();
        list.add((pageIndex-1)*pageSize);
        list.add(pageSize);

        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
            return null;
        }
        List<Menu>menuList = new ArrayList<>();
        try {
            while (resultSet.next()){
                Menu menu = new Menu();
                menu.setDesc(resultSet.getString("desc"));
                menu.setMenuId(resultSet.getInt("menuid"));
                menu.setState(resultSet.getInt("state"));
                menuList.add(menu);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return menuList;
    }
}

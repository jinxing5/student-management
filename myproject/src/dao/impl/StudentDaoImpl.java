package dao.impl;

import bean.Grade;
import bean.Student;
import dao.DBUtils;
import dao.StudentDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl extends DBUtils implements StudentDao {
    @Override
    public List<Student> GetAllStudent(String name,String stuno,int sex,int pageindex,int pagesize) {

        //运用StringBuffer进行 sql命令动态查询
        StringBuffer sql = new StringBuffer(" select * from student where 1=1 and state=1 ");
        List list = new ArrayList();
        if(name!=null&&name.length()>0){
            sql.append(" and stuname like? ");
            //模糊查需要拼接 % 或 _
            list.add("%"+name+"%");
        }
        if(stuno!=null&&stuno.length()>0){
            sql.append(" and stuno=? ");
            list.add(stuno);
        }
        if(sex!=-1){
            sql.append(" and sex=? ");
            list.add(sex);
        }
        //传入页码值
        sql.append(" limit ?,? ");
        list.add((pageindex-1)*pagesize);
        list.add(pagesize);

        getConnection();
        resultSet = query(sql.toString(),list);
        List<Student> students = new ArrayList<>();
        if(resultSet==null){
            closeAll();
            return null;
        }
        try {
            while (resultSet.next()){
                Student student = new Student();
                student.setStuname(resultSet.getString("stuname"));
                student.setAddress(resultSet.getString("address"));
                student.setEmail(resultSet.getString("email"));
                student.setGid(resultSet.getInt("gid"));
                student.setStuid(resultSet.getInt("stuid"));
                student.setIdnumber(resultSet.getString("idnumber"));
                student.setStuno(resultSet.getString("stuno"));
                student.setIntroduction(resultSet.getString("introduction"));
                student.setPolitics(resultSet.getString("politics"));
                student.setPhone(resultSet.getString("phone"));
                student.setProfession(resultSet.getString("profession"));
                student.setRegdate(resultSet.getDate("regdata"));
                student.setSex(resultSet.getInt("sex"));
                student.setState(resultSet.getInt("state"));
                student.setRegistered(resultSet.getString("registered"));
                students.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return students;
    }

    @Override
    public int DataCount(String name,String stuno,int sex) {
        //运用StringBuffer进行 sql命令动态查询
        StringBuffer sql = new StringBuffer(" select count(*) from student where 1=1 and state=1 ");
        List list = new ArrayList();
        if(name!=null&&name.length()>0){
            sql.append(" and stuname like? ");
            //模糊查需要拼接 % 或 _
            list.add("%"+name+"%");
        }
        if(stuno!=null&&stuno.length()>0){
            sql.append(" and stuno=? ");
            list.add(stuno);
        }
        if(sex!=-1){
            sql.append(" and sex=? ");
            list.add(sex);
        }

        int count = 0;
        getConnection();
        resultSet = query(sql.toString(),list);
        if(resultSet==null){
            closeAll();
            return count;
        }
        try {
            while (resultSet.next()){
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeAll();
        return count;
    }

    @Override
    public Student FindById(int stuid) {
        String sql = "select * from student where stuid=? ";
        List list = new ArrayList();
        list.add(stuid);
        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
            return null;
        }
        Student student = new Student();
        try {
            while (resultSet.next()){
                student.setStuname(resultSet.getString("stuname"));
                student.setAddress(resultSet.getString("address"));
                student.setEmail(resultSet.getString("email"));
                student.setGid(resultSet.getInt("gid"));
                student.setStuid(resultSet.getInt("stuid"));
                student.setIdnumber(resultSet.getString("idnumber"));
                student.setStuno(resultSet.getString("stuno"));
                student.setIntroduction(resultSet.getString("introduction"));
                student.setPolitics(resultSet.getString("politics"));
                student.setPhone(resultSet.getString("phone"));
                student.setProfession(resultSet.getString("profession"));
                student.setRegdate(resultSet.getDate("regdata"));
                student.setSex(resultSet.getInt("sex"));
                student.setState(resultSet.getInt("state"));
                student.setRegistered(resultSet.getString("registered"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return student;
    }

    @Override
    public List<Grade> GetGradeList() {
        String sql = "select * from grade";
        List<Grade> gradeList = new ArrayList<>();
        List list = new ArrayList();
        resultSet = query(sql,list);
        if(resultSet == null){
            closeAll();
            return null;
        }
        try {
            while (resultSet.next()){
                Grade grade = new Grade();
                grade.setGradeid(resultSet.getInt("gradeid"));
                grade.setGradename(resultSet.getString("gradename"));
                gradeList.add(grade);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            closeAll();
        }
        return gradeList;
    }

    @Override
    public boolean UpdateStudent(Student student) {
        String sql = "update student set" +
                " stuname=?,stuno=?,sex=?,phone=?,email=?,registered=?,address=?," +
                "profession=?,idnumber=?,politics=?,introduction=?,gid=?" +
                " where stuid=?";
        List list = new ArrayList();

        list.add(student.getStuname());
        list.add(student.getStuno());
        list.add(student.getSex());
        list.add(student.getPhone());
        list.add(student.getEmail());
        list.add(student.getRegistered());
        list.add(student.getAddress());
        list.add(student.getProfession());
        list.add(student.getIdnumber());
        list.add(student.getPolitics());
        list.add(student.getIntroduction());
        list.add(student.getGid());

        list.add(student.getStuid());

        getConnection();
        int count = update(sql,list);
        if(count==1) {
            closeAll();
            return true;
        }
        closeAll();
        return false;
    }

    /**
     * 假删除：将学生state改为 状态4：删除
     * @param stuid
     * @return
     */
    @Override
    public boolean DeleteStudent(int stuid) {
        String sql = "update student set state=4 where stuid=? ";
        List list =new ArrayList();
        list.add(stuid);
        getConnection();
        int count = update(sql,list);
        if(count==1){
            closeAll();
            return true;
        }
        closeAll();
        return false;
    }

    @Override
    public boolean AddStudent(Student student) {
        String sql = "insert into student values(null,?,?,?,?,?,?,?,?,?,?,null,1,?,?)";

        List list = new ArrayList();

        list.add(student.getStuname());
        list.add(student.getStuno());
        list.add(student.getSex());
        list.add(student.getPhone());
        list.add(student.getEmail());
        list.add(student.getRegistered());
        list.add(student.getAddress());
        list.add(student.getProfession());
        list.add(student.getIdnumber());
        list.add(student.getPolitics());
        list.add(student.getIntroduction());
        list.add(student.getGid());

        getConnection();
        int count = update(sql,list);
        if(count==1){
            closeAll();
            return true;
        }
        closeAll();
        return false;
    }

    @Override
    public List<Integer> GetStunoList() {
        String sql = "select stuno from student";
        List list = new ArrayList();
        getConnection();
        List stunolist = new ArrayList();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
            return null;
        }
        try {
            while (resultSet.next()){
                stunolist.add(resultSet.getInt("stuno"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return stunolist;
    }
}

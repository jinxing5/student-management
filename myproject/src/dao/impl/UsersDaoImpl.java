package dao.impl;

import bean.Menu;
import bean.Role;
import bean.Users;
import dao.DBUtils;
import dao.UsersDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersDaoImpl extends DBUtils implements UsersDao {
    @Override
    public Users askLoginDao(String name, String password) {
        String sql = "select * from users u,role r where u.roleid=r.roleid and u.loginName=? and u.password=? and rolestate=1 ";
        List list = new ArrayList();
        list.add(name);
        list.add(password);
        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
            return null;
        }
        Users user = null;
        try {
            while (resultSet.next()){
                user = new Users();
                user.setLoginName(resultSet.getString("loginName"));
                user.setUserid(resultSet.getInt("userid"));
                user.setPassword(resultSet.getString("password"));
                user.setRealname(resultSet.getString("realname"));
                user.setRoleid(resultSet.getInt("roleid"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return user;
    }

    @Override
    public List<Users> GetAllUser(int pageindex,int pagesize) {
        String sql = "select userid,realname,rolename,loginName from users u,role r WHERE u.roleid=r.roleid limit ?,?";
        List list = new ArrayList();
        list.add((pageindex-1)*pagesize);
        list.add(pagesize);

        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
            return null;
        }

        List<Users> users = new ArrayList<>();
        try {
            while (resultSet.next()){
                Users user = new Users();
                user.setUserid(resultSet.getInt("userid"));
                user.setRealname(resultSet.getString("realname"));
                user.setLoginName(resultSet.getString("loginName"));
                Role role = new Role();
                role.setRolename(resultSet.getString("rolename"));
                user.setRole(role);
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return users;
    }

    @Override
    public int CountUsers() {
        String sql = "select count(*) from users u,role r WHERE u.roleid=r.roleid";
        List list = new ArrayList();

        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
        }

        int count = 0;
        try {
            while (resultSet.next()){
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return count;
    }

    @Override
    public List<Role> GetRoleList() {
        String sql = "select * from role";
        List list = new ArrayList();
        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
            return null;
        }
        List<Role> roleList = new ArrayList<>();
        try {
            while (resultSet.next()){
                Role role = new Role();
                role.setState(resultSet.getInt("rolestate"));
                role.setRoleId(resultSet.getInt("roleid"));
                role.setRolename(resultSet.getString("rolename"));
                roleList.add(role);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return roleList;
    }

    @Override
    public boolean DeleteUser(int userid) {
        String sql = "delete from users where userid=? ";
        List list =new ArrayList();
        list.add(userid);
        getConnection();
        int count = update(sql,list);
        if(count==1){
            closeAll();
            return true;
        }
        closeAll();
        return false;
    }

    @Override
    public Role FindMenuByUserId(int roleid) {
        String sql = "SELECT * from role r,menu n,middle m WHERE r.roleid=m.roleid and n.menuid=m.menuid and r.roleid=?";
        List list = new ArrayList();
        list.add(roleid);

        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            return null;
        }

        Role role = new Role();
        List<Menu> menuList = new ArrayList<>();
        try {
            while (resultSet.next()){
                role.setRoleId(resultSet.getInt("roleid"));
                role.setState(resultSet.getInt("rolestate"));
                role.setRolename(resultSet.getString("rolename"));

                Menu menu = new Menu();
                menu.setMenuId(resultSet.getInt("menuid"));
                menu.setMenuName(resultSet.getString("menuname"));
                menu.setUrl(resultSet.getString("url"));
                menu.setUpmenu(resultSet.getInt("upmenuid"));
                menuList.add(menu);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        role.setMenus(menuList);
        return role;
    }


    @Override
    public boolean AddUser(Users user) {
        String sql = "insert into users values(null,?,?,?,?,?,?,?,?,?,?)";

        List list = new ArrayList();

        list.add(user.getLoginName());
        list.add(user.getPassword());
        list.add(user.getRealname());
        list.add(user.getSex());
        list.add(user.getEmail());
        list.add(user.getAddress());
        list.add(user.getPhone());
        list.add(user.getCardid());
        list.add(user.getDesc());
        list.add(user.getRoleid());

        getConnection();
        int count = update(sql,list);
        if(count==1){
            closeAll();
            return true;
        }
        closeAll();
        return false;
    }

    @Override
    public Users FindById(int userId) {
        String sql = "select * from users where userid=?";
        List list = new ArrayList();
        list.add(userId);

        Users user = new Users();
        getConnection();
        resultSet = query(sql,list);
        if(resultSet==null){
            closeAll();
            return null;
        }
        List<Role> roleList = new ArrayList<>();
        try {
            while (resultSet.next()){

                user.setUserid(resultSet.getInt("userid"));
                user.setRealname(resultSet.getString("realname"));
                user.setLoginName(resultSet.getString("loginName"));
                user.setPhone(resultSet.getString("phone"));
                user.setSex(resultSet.getInt("sex"));
                user.setDesc(resultSet.getString("desc"));
                user.setCardid(resultSet.getString("cardid"));
                user.setPassword(resultSet.getString("password"));
                user.setRoleid(resultSet.getInt("roleid"));
                user.setEmail(resultSet.getString("email"));
                user.setAddress(resultSet.getString("address"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeAll();
        }
        return user;
    }

    @Override
    public boolean UpdateUser(Users user) {
        String sql = "update users set loginname=?,password=?,realname=?,sex=?,email=?,address=?,phone=?,cardid=?,`desc`=?,roleid=? where userid=?";

        List list = new ArrayList();

        list.add(user.getLoginName());
        list.add(user.getPassword());
        list.add(user.getRealname());
        list.add(user.getSex());
        list.add(user.getEmail());
        list.add(user.getAddress());
        list.add(user.getPhone());
        list.add(user.getCardid());
        list.add(user.getDesc());
        list.add(user.getRoleid());

        list.add(user.getUserid());

        getConnection();
        int count = update(sql,list);
        if(count==1){
            closeAll();
            return true;
        }
        closeAll();
        return false;
    }
}

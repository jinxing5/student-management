package dao;

import bean.Menu;
import bean.Role;

import java.util.List;

public interface RoleDao {
    public boolean UpdateRole(Role role);
    public boolean AddRole(Role role);
    public int CountRole();
    public Role FindRoleById(int roleid);
    public List<Role> GetRoleList(int pageIndex, int pageSize);
    public List<Menu> GetMList();
    public boolean DeleteRole(int roleid);
    public boolean DisabedRole(int roleid,int state);
}

package dao;

import bean.Role;
import bean.Users;

import java.util.List;

public interface UsersDao {
    public Users askLoginDao(String name,String password);

    public List<Users> GetAllUser(int pageindex,int pagesize);
    public int CountUsers();
    public boolean AddUser(Users user);
    public Users FindById(int userId);
    public boolean UpdateUser(Users user);
    public List<Role> GetRoleList();
    public boolean DeleteUser(int userid);
    public Role FindMenuByUserId(int roleid);
}

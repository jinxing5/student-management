package dao;

import bean.Menu;
import bean.Role;

import java.util.List;

public interface MenuDao {
    public boolean UpdateMenu(Menu menu);
    public boolean AddMenu(Menu menu);
    public int CountMenu();
    public Role FindMenuById(int menuid);
    public List<Menu> GetMenuList(int pageIndex, int pageSize);
}

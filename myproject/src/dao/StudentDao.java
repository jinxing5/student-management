package dao;

import bean.Grade;
import bean.Student;

import java.util.List;

public interface StudentDao {
    public List<Student> GetAllStudent(String name,String stuno,int sex,int pageindex,int pagesize);
    public int DataCount(String name,String stuno,int sex);
    public Student FindById(int stuid);
    public List<Grade> GetGradeList();
    public boolean UpdateStudent(Student student);
    public boolean DeleteStudent(int stuid);
    public boolean AddStudent(Student student);
    public List<Integer> GetStunoList();
}

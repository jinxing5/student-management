package bean;

import java.util.List;

public class Role {
    private Integer roleId;
    private String rolename;
    private Integer state;
    private List<Users> users;
    private List<Menu> menus;

    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + roleId +
                ", rolename='" + rolename + '\'' +
                ", state=" + state +
                ", users=" + users +
                ", menus=" + menus +
                '}';
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public List<Users> getUsers() {
        return users;
    }

}

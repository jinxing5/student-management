package bean;

import java.util.List;

public class Menu {
    private Integer menuId;
    private String menuName;
    private Integer upmenu;
    private Integer state;
    private String desc;
    private String url;
    private List<Users> users;
    private List<Menu> secondMenus; //保存二级目录

    public List<Menu> getSecondMenus() {
        return secondMenus;
    }

    public void setSecondMenus(List<Menu> secondMenus) {
        this.secondMenus = secondMenus;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Integer getUpmenu() {
        return upmenu;
    }

    public void setUpmenu(Integer upmenu) {
        this.upmenu = upmenu;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }
}

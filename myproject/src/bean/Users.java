package bean;

public class Users {
    private Integer userid;
    private String loginName;
    private String password;
    private String realname;
    private Integer sex;
    private String email;
    private String address;
    private String phone;
    private String cardid;
    private String desc;
    private Integer roleid;
    private Role role;

    public Users(Integer userid, String loginName, String password, String realname, Integer sex, String email, String address, String phone, String cardid, String desc, Integer roleid, Role role) {
        this.userid = userid;
        this.loginName = loginName;
        this.password = password;
        this.realname = realname;
        this.sex = sex;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.cardid = cardid;
        this.desc = desc;
        this.roleid = roleid;
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Users() {
    }
}

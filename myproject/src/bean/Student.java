package bean;

import java.util.Date;

public class Student {
    private int stuid;
    private String stuname;
    private String stuno;
    private int sex;
    private String phone;
    private String email;
    private String registered;
    private String address;
    private String profession;
    private String idnumber;
    private String politics;
    private java.sql.Date regdate;
    private int state;
    private String introduction;
    private int gid;
    private Grade grade;

    public int getStuid() {
        return stuid;
    }

    public void setStuid(int stuid) {
        this.stuid = stuid;
    }

    public String getStuname() {
        return stuname;
    }

    public void setStuname(String stuname) {
        this.stuname = stuname;
    }

    public String getStuno() {
        return stuno;
    }

    public void setStuno(String stuno) {
        this.stuno = stuno;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public String getPolitics() {
        return politics;
    }

    public void setPolitics(String politics) {
        this.politics = politics;
    }

    public Date getRegdate() {
        return regdate;
    }

    public void setRegdate(java.sql.Date regdate) {
        this.regdate = regdate;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public Student(int stuid, String stuname, String stuno, int sex, String phone, String email, String registered, String address, String profession, String idnumber, String politics, java.sql.Date regdate, int state, String introduction, int gid, Grade grade) {
        this.stuid = stuid;
        this.stuname = stuname;
        this.stuno = stuno;
        this.sex = sex;
        this.phone = phone;
        this.email = email;
        this.registered = registered;
        this.address = address;
        this.profession = profession;
        this.idnumber = idnumber;
        this.politics = politics;
        this.regdate = regdate;
        this.state = state;
        this.introduction = introduction;
        this.gid = gid;
        this.grade = grade;
    }

    public Student() {
    }
}

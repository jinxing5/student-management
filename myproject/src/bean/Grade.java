package bean;

import java.util.List;

public class Grade {
    private Integer gradeid;
    private String gradename;
    private List<Student> students;

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Grade(Integer gradeid, String gradename, List<Student> students) {
        this.gradeid = gradeid;
        this.gradename = gradename;
        this.students = students;
    }

    public Integer getGradeid() {
        return gradeid;
    }

    public void setGradeid(Integer gradeid) {
        this.gradeid = gradeid;
    }

    public String getGradename() {
        return gradename;
    }

    public void setGradename(String gradename) {
        this.gradename = gradename;
    }


    public Grade() {
    }
}

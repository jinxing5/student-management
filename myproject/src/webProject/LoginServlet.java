package webProject;

import bean.Users;
import net.sf.json.JSONObject;
import service.UsersService;
import service.impl.UsersServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/askLogin")
public class LoginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.接收数据
        String name = req.getParameter("TxtUserName");
        String password = req.getParameter("TxtPassword");
        //2.调用服务
        Users user = new Users();
        UsersService usersService = new UsersServiceImpl();
        user = usersService.askLoginService(name,password);
        //3.返回数据
        if(user==null){
            //弹窗提示
            PrintWriter writer = resp.getWriter();
            resp.setContentType("text/html;charset=utf-8;");
            writer.println("<script>location.href='login.jsp';alert('用户名或密码不正确！');</script>");
        }else {
            //保存用户信息
            req.getSession().setAttribute("user1",user);
            //跳转到主页面
            resp.sendRedirect("index.jsp");
        }
    }
}

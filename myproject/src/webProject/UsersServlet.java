package webProject;

import bean.Role;
import bean.Student;
import bean.Users;
import service.StudentService;
import service.UsersService;
import service.impl.StudentServiceImpl;
import service.impl.UsersServiceImpl;
import util.PageUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = "/power/user/userServlet")
public class UsersServlet extends HttpServlet {
    private UsersService usersService = new UsersServiceImpl();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getParameter("method");
        switch (method){
            case "getUList":
                GetUserList(req, resp);
                break;
            case "getRoleList":
                GetRoleList(req, resp);
                break;
            case "addUser":
                AddUser(req, resp);
                break;
            case "updateUser":
                UpdateUser(req, resp);
                break;
            case "findById":
                FindById(req, resp);
                break;
            case "delete":
                Delete(req, resp);
                break;
            default:
                break;
        }

    }

    protected void GetUserList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PageUtil pageUtil = new PageUtil();
        int pageIndex = req.getParameter("pageindex")==null?1:Integer.parseInt(req.getParameter("pageindex"));
        pageUtil.setPageIndex(pageIndex);

        List<Users> usersList = usersService.GetAllUser(pageUtil.getPageIndex(),pageUtil.getPageSize());
        pageUtil.setTotal(usersService.CountUsers());

        req.setAttribute("pageInfo",pageUtil);
        req.setAttribute("userlist",usersList);
        req.getRequestDispatcher("list.jsp").forward(req,resp);
    }

    protected void AddUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Users user = new Users();

        user.setLoginName(req.getParameter("loginName"));
        user.setRoleid(Integer.parseInt(req.getParameter("roleId")));
        user.setSex(Integer.parseInt(req.getParameter("sex")));
        user.setPassword(req.getParameter("password"));
        user.setCardid(req.getParameter("cardid"));
        user.setRealname(req.getParameter("realname"));
        user.setEmail(req.getParameter("email"));
        user.setAddress(req.getParameter("address"));
        user.setDesc(req.getParameter("desc"));
        user.setPhone(req.getParameter("phone"));
        user.setCardid(req.getParameter("cardid"));

        boolean flag = usersService.AddUser(user);
        PrintWriter writer = resp.getWriter();
        if(flag) {
            writer.println("<script>alert('添加成功！');location.href='/power/user/userServlet?method=getUList'</script>");
        }else {
            writer.println("<script>alert('添加失败！');location.href='/power/user/userServlet?method=getUList'</script>");
        }
    }

    protected void FindById(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Users user = usersService.FindById(Integer.parseInt(req.getParameter("userid")));
        List<Role> roleList = usersService.GetRoleList();

        req.setAttribute("roleList",roleList);
        req.setAttribute("user",user);
        req.getRequestDispatcher("edit.jsp").forward(req,resp);
    }

    protected void Delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int userid = Integer.parseInt(req.getParameter("userid"));
        boolean flag = usersService.DeleteUser(userid);
        PrintWriter writer = resp.getWriter();
        if(flag) {
            writer.println("<script>alert('删除成功！');location.href='/power/user/userServlet?method=getUList'</script>");
        }else {
            writer.println("<script>alert('删除失败！');location.href='/power/user/userServlet?method=getUList'</script>");
        }
    }

    protected void UpdateUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Users user = new Users();

        user.setUserid(Integer.parseInt(req.getParameter("userid")));

        user.setLoginName(req.getParameter("loginName"));
        user.setRoleid(Integer.parseInt(req.getParameter("roleid")));
        user.setSex(Integer.parseInt(req.getParameter("sex")));
        user.setPassword(req.getParameter("password"));
        user.setCardid(req.getParameter("cardid"));
        user.setRealname(req.getParameter("realname"));
        user.setEmail(req.getParameter("email"));
        user.setAddress(req.getParameter("address"));
        user.setDesc(req.getParameter("desc"));
        user.setPhone(req.getParameter("phone"));
        user.setCardid(req.getParameter("cardid"));

        boolean flag = usersService.UpdateUser(user);
        PrintWriter writer = resp.getWriter();
        if(flag) {
            writer.println("<script>alert('修改成功！');location.href='/power/user/userServlet?method=getUList'</script>");
        }else {
            writer.println("<script>alert('修改失败！');location.href='/power/user/userServlet?method=getUList'</script>");
        }
    }

    protected void GetRoleList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Role> roleList = usersService.GetRoleList();
        req.setAttribute("roleList",roleList);
        req.getRequestDispatcher("add.jsp").forward(req,resp);
    }
}

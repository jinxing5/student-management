package webProject;

import bean.Menu;
import bean.Role;
import service.RoleService;
import service.UsersService;
import service.impl.RoleServiceImpl;
import service.impl.UsersServiceImpl;
import util.PageUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/power/role/GetRole")
public class RoleServlet extends HttpServlet {
    private RoleService roleService = new RoleServiceImpl();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getParameter("method");
        switch (method){
            case "GetRList":
                GetRoleList(req, resp);
                break;
            case "addGetMList":
                AddGetMenuList(req, resp);
                break;
            case "editGetMList":
                EditGetMenuList(req, resp);
                break;
            case "disabledRole":
                DisabledRole(req, resp);
                break;
            case "addRole":
                AddRole(req, resp);
                break;
            case "findById":
                FindById(req, resp);
                break;
            case "deleteRole":
                DeleteRole(req, resp);
                break;
            case "UpdateRole":
                UpdateRole(req, resp);
                break;
            default:
                break;
        }
    }

    protected void GetRoleList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PageUtil pageUtil = new PageUtil();
        int pageIndex = req.getParameter("pageindex")==null?1:Integer.parseInt(req.getParameter("pageindex"));
        pageUtil.setPageIndex(pageIndex);

        List<Role> roleList = roleService.GetRoleList(pageUtil.getPageIndex(),pageUtil.getPageSize());
        pageUtil.setTotal(roleService.CountRole());

        req.setAttribute("pageInfo",pageUtil);
        req.setAttribute("roleList",roleList);
        req.getRequestDispatcher("list.jsp").forward(req,resp);
    }

    protected void AddGetMenuList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Menu> menuList = roleService.GetMList();

        req.setAttribute("menuList",menuList);
        req.getRequestDispatcher("add.jsp").forward(req,resp);

    }

    protected void EditGetMenuList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int roleid = Integer.parseInt(req.getParameter("roleid"));

        Role role = roleService.FindRoleById(roleid);
        List<Menu> menuList = roleService.GetMList();

        req.setAttribute("role",role);
        req.setAttribute("menuList",menuList);
        req.getRequestDispatcher("edit.jsp").forward(req,resp);

    }

    protected void FindById(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int roleid = Integer.parseInt(req.getParameter("roleid"));
        Role role = roleService.FindRoleById(roleid);
        req.setAttribute("role",role);
        req.getRequestDispatcher("info.jsp").forward(req,resp);
    }

    protected void AddRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Role role = new Role();
        role.setRolename(req.getParameter("rolename"));
        role.setState(Integer.parseInt(req.getParameter("state")));
        String[] menuids = req.getParameterValues("menuid");

        List<Menu> menuList = new ArrayList<>();
        for (String menuid:menuids
             ) {
            Menu menu = new Menu();
            menu.setMenuId(Integer.parseInt(menuid));
            menuList.add(menu);
        }
        role.setMenus(menuList);

        boolean flag = roleService.AddRole(role);
        PrintWriter writer = resp.getWriter();
        if(flag) {
            writer.println("<script>alert('添加成功！');location.href='/power/role/GetRole?method=GetRList'</script>");
        }
    }

    protected void DeleteRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int roleId = Integer.parseInt(req.getParameter("roleid"));
        boolean flag = roleService.DeleteRole(roleId);
        PrintWriter writer = resp.getWriter();
        if(flag) {
            writer.println("<script>alert('删除成功！');location.href='/power/role/GetRole?method=GetRList'</script>");
        }else {
            writer.println("<script>alert('删除失败！');location.href='/power/role/GetRole?method=GetRList'</script>");
        }
    }

    protected void DisabledRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int roleId = Integer.parseInt(req.getParameter("roleid"));
        //根据前端 state 传入state变更状态操作：如果禁用：原state即为1，启用：0
        int state = Integer.parseInt(req.getParameter("state"))==1?0:1;
        boolean flag = roleService.DisabedRole(roleId,state);
        PrintWriter writer = resp.getWriter();
        if(flag) {
            writer.println("<script>alert('状态变更成功！');location.href='/power/role/GetRole?method=GetRList'</script>");
        }else {
            writer.println("<script>alert('状态变更失败！');location.href='/power/role/GetRole?method=GetRList'</script>");
        }
    }

    protected void UpdateRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Role role = new Role();
        role.setRoleId(Integer.parseInt(req.getParameter("roleid")));
        role.setRolename(req.getParameter("rolename"));
        role.setState(Integer.parseInt(req.getParameter("state")));
        String[] menuids = req.getParameterValues("menuid");

        List<Menu> menuList = new ArrayList<>();
        for (String menuid:menuids
        ) {
            Menu menu = new Menu();
            menu.setMenuId(Integer.parseInt(menuid));
            menuList.add(menu);
        }
        role.setMenus(menuList);

        boolean flag = roleService.UpdateRole(role);
        PrintWriter writer = resp.getWriter();
        if(flag) {
            writer.println("<script>alert('修改成功！');location.href='/power/role/GetRole?method=GetRList'</script>");
        }else {
            writer.println("<script>alert('修改失败！');location.href='/power/role/GetRole?method=GetRList'</script>");
        }
    }
}

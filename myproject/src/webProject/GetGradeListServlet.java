package webProject;

import bean.Grade;
import service.StudentService;
import service.impl.StudentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/Educational/student/GetGradeList")
public class GetGradeListServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StudentService studentService = new StudentServiceImpl();
        List<Grade> gradeList = new ArrayList<>();
        gradeList = studentService.GetGradeList();

        req.setAttribute("gradeList",gradeList);
        req.getRequestDispatcher("add.jsp").forward(req,resp);
    }
}

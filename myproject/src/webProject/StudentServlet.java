package webProject;

import bean.Grade;
import bean.Student;
import service.StudentService;
import service.impl.StudentServiceImpl;
import util.PageUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/Educational/student/StuServlet")
public class StudentServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getParameter("method");
        if (method==null) method="GetStuList";
        switch (method){
            case "Delete":
                DeleteStudent(req, resp);
                break;
            case "Update":
                UpdateStudent(req, resp);
                break;
            case "FindById":
                FindById(req, resp);
                break;
            case "AddStudent":
                AddStudent(req, resp);
                break;
            case "GetStuList":
                GetStudentList(req,resp);
                break;
        }
    }

    protected void DeleteStudent(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int stuid = Integer.parseInt(req.getParameter("stuid"));
        StudentService studentService = new StudentServiceImpl();
        boolean flag = studentService.DeleteStudent(stuid);
        PrintWriter writer = resp.getWriter();
        if(flag){
            writer.println("<script>alert('删除成功！');location.href='/Educational/student/StuServlet'</script>");
        }else {
            writer.println("<script>alert('删除失败！');location.href='/Educational/student/StuServlet'</script>");
        }
    }

    protected void FindById(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.接收数据
        String stuid = req.getParameter("stuid");
        //2.调用服务
        StudentService studentService = new StudentServiceImpl();
        Student student = studentService.FindById(Integer.parseInt(stuid));
        List<Grade> gradeList = studentService.GetGradeList();
        //3.返回数据
        req.setAttribute("gradeList",gradeList);
        req.setAttribute("student",student);
        req.getRequestDispatcher("edit.jsp").forward(req,resp);
    }

    protected void AddStudent(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Student student = new Student();

        student.setStuname(req.getParameter("stuname"));
        student.setRegistered(req.getParameter("registered"));
        student.setSex(Integer.parseInt(req.getParameter("sex")));
        student.setStuno(req.getParameter("stuno"));
        student.setProfession(req.getParameter("profession"));
        student.setIdnumber(req.getParameter("idnumber"));
        student.setGid(Integer.parseInt(req.getParameter("gid")));
        student.setEmail(req.getParameter("email"));
        student.setAddress(req.getParameter("address"));
        student.setPhone(req.getParameter("phone"));
        student.setPolitics(req.getParameter("politics"));
        student.setIntroduction(req.getParameter("introduction"));

        StudentService studentService = new StudentServiceImpl();
        boolean flag = studentService.AddStudent(student);
        PrintWriter writer = resp.getWriter();
        if(flag){
            writer.println("<script>alert('添加成功！');location.href='/Educational/student/StuServlet'</script>");
        }else {
            writer.println("<script>alert('添加失败！');location.href='/Educational/student/StuServlet'</script>");
        }
    }

    protected void UpdateStudent(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Student student = new Student();

        student.setStuid(Integer.parseInt(req.getParameter("stuid")));

        student.setStuname(req.getParameter("stuname"));
        student.setRegistered(req.getParameter("registered"));
        student.setSex(Integer.parseInt(req.getParameter("sex")));
        student.setStuno(req.getParameter("stuno"));
        student.setProfession(req.getParameter("profession"));
        student.setPhone(req.getParameter("phone"));
        student.setIdnumber(req.getParameter("idnumber"));
        student.setGid(Integer.parseInt(req.getParameter("gid")));
        student.setEmail(req.getParameter("email"));
        student.setAddress(req.getParameter("address"));
        student.setPhone(req.getParameter("phone"));
        student.setPolitics(req.getParameter("politics"));

        StudentService studentService = new StudentServiceImpl();
        boolean flag = studentService.UpdateStudent(student);
        PrintWriter writer = resp.getWriter();
        if(flag) {
            writer.println("<script>alert('修改成功！');location.href='/Educational/student/StuServlet'</script>");
        }else {
            writer.println("<script>alert('修改失败！');location.href='/Educational/student/StuServlet'</script>");
        }
    }

    protected void GetStudentList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.接收请求
        //1.1接收模糊查条件
        String name = req.getParameter("stuname");
        String sex = req.getParameter("sex");
        String stuno = req.getParameter("stuno");
        //1.2接收页码值
        PageUtil pageUtil = new PageUtil();
        String pageindex = req.getParameter("pageindex");
        String pagesize = req.getParameter("pagesize");
        //给页码信息赋初值
        if(pageindex == null) pageindex = "1";
        if(pagesize == null) pagesize = "5";

        //2.调用服务
        //2.1 查询 list 服务
        StudentService studentService = new StudentServiceImpl();
        List<Student> students = new ArrayList<>();
        if(sex==null) sex="-1";
        students = studentService.GetAllStudent(name,stuno,Integer.parseInt(sex),Integer.parseInt(pageindex),pageUtil.getPageSize());
        //2.2 查询页码服务
        int datacount =  studentService.DataCount(name,stuno,Integer.parseInt(sex));

        //3.返回数据
        System.out.println(name+sex+stuno);
        req.setAttribute("stuList",students);
        //保存查询的条件
        req.setAttribute("stuname",name);
        req.setAttribute("sex",sex);
        req.setAttribute("stuno",stuno);
        //保存页码值
        pageUtil.setTotal(datacount);
        pageUtil.setPageIndex(Integer.parseInt(pageindex));
        req.setAttribute("pageInfo",pageUtil);
        //此时的跳转实在请求路径之下的
        req.getRequestDispatcher("list.jsp").forward(req,resp);
    }
}

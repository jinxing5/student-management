package webProject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/logout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //清楚session
        req.getSession().invalidate();
        //跳转页面 使用了框架所以不能使用sendRedirect（使用了框架而一部分会跳转）所以应该使用js发送 top 跳转
        //resp.sendRedirect("login.jsp");
        resp.setContentType("text/html;charset=utf-8;");
        resp.getWriter().println("<script>alert('退出成功');top.location.href='login.jsp';</script>");
    }
}

package webProject;

import service.StudentService;
import service.impl.StudentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = "/Educational/student/nameCheck")
public class NameCheckServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int stuno = Integer.parseInt(req.getParameter("stuno"));

        StudentService studentService = new StudentServiceImpl();
        List<Integer> stunolist = studentService.GetStunoList();

        resp.setContentType("text/html;charset=utf-8;");
        String info = "学号可用！";
        PrintWriter writer = resp.getWriter();
        if(stunolist.contains(stuno)) info = "学号已存在！";
        writer.println(info);
    }
}

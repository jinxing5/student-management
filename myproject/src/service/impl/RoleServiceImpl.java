package service.impl;

import bean.Menu;
import bean.Role;
import dao.RoleDao;
import dao.impl.RoleDaoImpl;
import service.RoleService;

import java.util.ArrayList;
import java.util.List;

public class RoleServiceImpl implements RoleService {
    private RoleDao roleDao = new RoleDaoImpl();
    @Override
    public boolean UpdateRole(Role role) {
        return roleDao.UpdateRole(role);
    }

    @Override
    public boolean AddRole(Role role) {
        return roleDao.AddRole(role);
    }

    @Override
    public int CountRole() {
        return roleDao.CountRole();
    }

    @Override
    public Role FindRoleById(int roleid) {
        return roleDao.FindRoleById(roleid);
    }

    @Override
    public List<Role> GetRoleList(int pageIndex, int pageSize) {
        return roleDao.GetRoleList(pageIndex,pageSize);
    }

    @Override
    public List<Menu> GetMList() {
        List<Menu> menuList =  roleDao.GetMList();
        List<Menu> newmenuList = new ArrayList<>();

        for (Menu menu:menuList
             ) {
            if(menu.getUpmenu()==0){    //一级菜单
                List<Menu> secondMenuList = new ArrayList<>();
                for (Menu second:menuList
                     ) {
                    if(second.getUpmenu()==menu.getMenuId()){   //寻找一级菜单下的二级菜单
                        secondMenuList.add(second);
                    }
                }
                menu.setSecondMenus(secondMenuList);
                newmenuList.add(menu);
            }
        }

        return newmenuList;
    }

    @Override
    public boolean DisabedRole(int roleid,int state) {
        return roleDao.DisabedRole(roleid,state);
    }

    @Override
    public boolean DeleteRole(int roleid) {
        return roleDao.DeleteRole(roleid);
    }
}

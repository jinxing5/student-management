package service.impl;

import bean.Menu;
import bean.Role;
import bean.Users;
import dao.UsersDao;
import dao.impl.UsersDaoImpl;
import service.UsersService;

import java.util.ArrayList;
import java.util.List;

public class UsersServiceImpl implements UsersService {
    private UsersDao usersDao =new  UsersDaoImpl();

    @Override
    public Users askLoginService(String name, String password) {
        Users user = usersDao.askLoginDao(name,password);
        //从数据库取出完整的用户信息；包含角色、菜单信息
        if(user==null) return null;
        Role role = usersDao.FindMenuByUserId(user.getRoleid());
        List<Menu> menuList = role.getMenus();

        List<Menu> newMenuList = new ArrayList<>();
        for (Menu menu:menuList) {
            if(menu.getUpmenu()==0){    //一级菜单
                List<Menu> secondMenuList = new ArrayList<>();
                for (Menu second:menuList) {
                    if(second.getUpmenu()==menu.getMenuId()){   //寻找一级菜单下的二级菜单
                        secondMenuList.add(second);
                    }
                }
                menu.setSecondMenus(secondMenuList);
                newMenuList.add(menu);
            }
        }
        role.setMenus(newMenuList);
        user.setRole(role);
        return user;
    }

    @Override
    public List<Users> GetAllUser(int pageindex,int pagesize) {
        return usersDao.GetAllUser(pageindex, pagesize);
    }

    @Override
    public int CountUsers() {
        return usersDao.CountUsers();
    }

    @Override
    public List<Role> GetRoleList() {
        return usersDao.GetRoleList();
    }

    @Override
    public boolean AddUser(Users user) {
        return usersDao.AddUser(user);
    }

    @Override
    public Users FindById(int userId) {
        return usersDao.FindById(userId);
    }

    @Override
    public boolean UpdateUser(Users user) {
        return usersDao.UpdateUser(user);
    }

    @Override
    public boolean DeleteUser(int userid) {
        return usersDao.DeleteUser(userid);
    }
}

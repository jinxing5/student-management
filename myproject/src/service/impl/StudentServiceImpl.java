package service.impl;

import bean.Grade;
import bean.Student;
import dao.StudentDao;
import dao.impl.StudentDaoImpl;
import service.StudentService;

import java.util.List;

public class StudentServiceImpl implements StudentService {
    private StudentDao studentDao = new StudentDaoImpl();
    @Override
    public List<Student> GetAllStudent(String name,String stuno,int sex,int pageindex,int pagesize) {
        return studentDao.GetAllStudent(name,stuno,sex,pageindex,pagesize);
    }

    @Override
    public int DataCount(String name, String stuno, int sex) {
        return studentDao.DataCount(name,stuno,sex);
    }

    @Override
    public Student FindById(int stuid) {
        return studentDao.FindById(stuid);
    }

    @Override
    public List<Grade> GetGradeList() {
        return studentDao.GetGradeList();
    }

    @Override
    public boolean UpdateStudent(Student student) {
        return studentDao.UpdateStudent(student);
    }

    @Override
    public boolean DeleteStudent(int stuid) {
        return studentDao.DeleteStudent(stuid);
    }

    @Override
    public boolean AddStudent(Student student) {
        return studentDao.AddStudent(student);
    }

    @Override
    public List<Integer> GetStunoList() {
        return studentDao.GetStunoList();
    }

}

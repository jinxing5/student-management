package service;

import bean.Role;
import bean.Users;

import java.util.List;

public interface UsersService {
    public Users askLoginService(String name,String password);
    public List<Users> GetAllUser(int pageindex,int pagesize);
    public int CountUsers();
    public List<Role> GetRoleList();
    public boolean AddUser(Users user);
    public Users FindById(int userId);
    public boolean UpdateUser(Users user);
    public boolean DeleteUser(int userid);
}

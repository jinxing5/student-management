package util;

import javax.validation.ReportAsSingleViolation;

public enum StudentStateEnum{
    READING(1,"在读"),
    SUSPENSION(2,"休学"),
    DROPOUT(3,"退学"),
    GRADUATE(4,"毕业"),
    DELETE(5,"删除");
    public final Integer type;
    public final String value;
    StudentStateEnum(Integer type,String value) {
        this.type=type;
        this.value=value;
    }
}

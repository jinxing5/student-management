# 学生管理

#### 介绍
这是一个 Java Web 项目
简易的学生管理系统

#### 软件架构
功能简介：
- 已注册用户登录、退出
- 老师管理学生信息
- 管理员管理用户权限


#### 安装教程

1.  Tomcat版本：8.5.34
2.  MySQL8
3.  Java 11.0.11
4.  下载即可使用！

#### 使用说明

1.  先在本地数据库中创建数据库！根据 src 目录的 sql 文件和 excel 文件创建数据库。
2.  使用 jdbc工具类DBUtils 进行数据库的连接 数据库连接配置文件在src 目录下：需要改成自己的数据库登录账户与密码！
3.  在网页中键入 http://localhost:8080/ 登录系统

#### 参与贡献
作者：Venus枯草